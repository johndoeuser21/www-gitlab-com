---
layout: markdown_page
title: "TMRG - GitLab Pride"
description: "We are the GitLab Pride Team Member Resource Group (TMRG) founded in the fall of 2019. Learn more!"
canonical_path: "/company/culture/inclusion/erg-gitlab-pride/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction

This group strives to connect employees at GitLab that are part of the LGBTQ+ community, or are allies, with professional and personal opportunities to meet others, speak at events, and share their lived experiences to improve and strengthen our community.

## Mission

To ensure that GitLab is proactive in supporting, retaining, and recruiting from the LGBTQ+ community. This group also aims to help develop and coordinate mentoring in the workplace.

## Leads
* [Alex Hanselka](https://about.gitlab.com/company/team/#ahanselka) 
* [Helen Mason](https://about.gitlab.com/company/team/#hmason)


## Executive Sponsor
* [Robin Schulman](https://about.gitlab.com/company/team/#rschulman)

## Upcoming Events
* [Coming Out Day Panel Discussion Recording](https://youtu.be/OUKWs6hkMQY)

## Additional Resources

- Sign up to get meeting invites by joining the [GitLab Pride Google Group](https://groups.google.com/a/gitlab.com/g/pride-tmrg)
- [GitLab Pride Issue Board](https://gitlab.com/gitlab-com/pride-tmrg/)

## Resources for Being an Ally with the Pride Community
- Gladd.org: [10 Ways to Be an Ally & a Friend](https://www.glaad.org/resources/ally/2)
- TechStars: Out At Work: [How To Support And Be An Ally To LGBTQ+ Colleagues](https://www.techstars.com/the-line/advice/out-at-work-how-to-support-and-be-an-ally-to-lgbtq-colleagues)
- Forbes: [How To Be An LGBTQ Ally At Work](https://www.forbes.com/sites/brianhonigman/2016/07/20/lgbtq-ally-at-work/#30a6ee0142fc)
- Vox: [Here’s what a good LGBTQ ally looks like](https://www.vox.com/identities/2019/6/22/18700875/lgbtq-good-ally)
- PFLAG’s [Guide to being a straight ally](https://pflag.org/sites/default/files/4th%20Edition%20Guide%20to%20Being%20an%20Ally.pdf)
- Oprah’s [Guide to being an LGBTQ Ally](https://www.oprahmag.com/life/relationships-love/a28159555/how-to-be-lgbtq-ally/)
