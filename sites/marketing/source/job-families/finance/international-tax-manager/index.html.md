---
layout: job_family_page
title: "Tax Manager"
---

## International Tax Manager 

## Levels

### International Tax Manager (Intermediate)

The International Tax Manager (Intermediate) reports to [Director of Tax](/job-families/finance/director-of-tax/).

#### International Tax Manager (Intermediate) Job Grade 

The International Tax Manager (Intermediate) is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### International Tax Manager (Intermediate) Responsibilities

* Play a significant role in establishing GitLab’s global tax strategy
* Tax Planning
  * Support the implementation of non-structural tax planning strategies
  * Participate in proposed Transactions to advise the Finance Team on Tax Consequences
  * Support USA and foreign tax integration of acquisitions, dispositions and restructurings into the corporate structure
* Transfer Pricing
  * Ensure adherence to the Transfer Pricing Concept
  * Maintain Transfer Pricing Documentation
* Tax Compliance
  * Ensure Global Corporate / State Income Tax Compliance
  * Closely work together with finance team members on VAT, Sales, Use Tax compliance
  * Ensure Global VAT compliance to electronic services
  * Take ownership of the control cycle to mitigate risks of Permanent Establishments
  * Take ownership of the cycle to mitigate employment tax risks
* Tax Audits
  * Evaluate requests and participate in developing strategies
  * Support the coordination and implementation of tax audit strategies
* Financial Reporting
  * Support preparation current / deferred income tax expense for financial statement purposes
  * Evaluate uncertain tax reporting positions and business combinations
  * Analyze the tax aspects of financial projections and forecasts
  * Lead technology and process improvements to ensure accurate, timely information is provided to the Finance team
  * Implementation and assurance of control cycle compliant with Sarbanes-Oxley
* Departmental liaison with IT staff on technical matters relating to tax applications

#### International Tax Manager (Intermediate) Requirements 

* Degree in Business Taxation (Master's / JD / CPA)
* Proven track record of personal growth in your career path
* 5-8 years experience in a Big 4 environment and/or Business environment
* Affinity with Software and/or SaaS in a high growth environment
* Demonstrate a hands-on approach and success in working in a team environment
* Working knowledge of multiple International tax areas
* Ability to managing multiple projects at the same time
* Ability to use GitLab

### Senior International Tax Manager

The Senior International Tax Manager reports to [Director of Tax](/job-families/finance/director-of-tax/).

#### Senior International Tax Manager Job Grade 

The Senior International Tax Manager is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Senior International Tax Manager Responsibilities

* Extends that of the International Tax Manager (Intermediate) responsibilities

#### Senior International Tax Manager Requirements 

* Extends that of the nternational Tax Manager (Intermediate) requirements
* Degree in Business Taxation (Master's / JD / CPA)
* 8-12 years experience in a Big 4 environment and Business environment
* Successfully leading large projects and/or change management projects
* Strong influencing skills, high adaptability and analytical thinking
* Expert in multiple International tax areas
* Leading multiple projects at the same time

### Staff International Tax Manager

The Staff International Tax Manager reports to [Director of Tax](/job-families/finance/director-of-tax/).

#### Staff International Tax Manager Job Grade

The Staff International Tax Manager is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Staff International Tax Manager Responsibilities

* Extends that of the Senior International Tax Manager responsibilities

#### Staff International Tax Manager Requirements

* Extends that of the Senior International Tax Manager requirements
* Degree in Business Taxation (Master's / JD / CPA)
* 15+ years experience in a Big 4 environment and Business environment
* Strong business judgment applied to tax and finance operational activities
* Proven international experience working with overseas operations
* Proven success with managing tax on a global scale within a fast pace environment
* Proven ability to drive change in tax processes with a technology driven mindset
* Demonstrated experience in mentoring and leading a distributed team
* Leadership experience in a software or global technology company
* Proven strategic and tactical vision to lead a high performing team

## US Tax Manager 

## Levels

### US Tax Manager (Intermediate)

The US Tax Manager (Intermediate) reports to [Director of Tax](/job-families/finance/director-of-tax/).

#### US Tax Manager (Intermediate) Job Grade
The US Tax Manager (Intermediate) is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### US Tax Manager (Intermediate) Responsibilities

- Contribute to GitLab’s Global Tax Strategy
- Contribute to Global Tax Planning Strategies
- Contribute to USA Tax integration of acquisitions, dispositions and restructurings
- Contribute to the adherence to GitLab’s Transfer Pricing Model
- Ownership of US Federal, State and Local Tax Compliance
- Collaborate with our Tax Accountant on USA Sales & Use Tax Compliance
- Contribute to the mitigation of Permanent Establishment exposure
- Contribute to the mitigation of Employment Tax exposure
- Contribute to US Tax Audits and participate in developing strategies
- Contribute to the US and Foreign Tax Provisioning
- Contribute to the maintenance of GitLab’s Global Corporate Structure
- Evaluate and perform calculations for uncertain tax position accruals (e.g. FIN48, FAS5)
- Analyze the tax aspects of GitLab’s Financial Model (e.g. BEAT, GILTI, FDII)
- Collaborate with the Finance Organization (Accounting, Treasury Reporting & Compliance)
- Collaborate with the Legal Organization (e.g. corporate structure, registrations)
- Collaborate with Global Sales Team to Withholding Tax Related Questions
- Collaborate with Payroll & Stock Administrator on US tax implications of stock options 
- Contribute to technology improvements to ensure fast and accurate data processing
- Implementation and assurance of control cycle compliant with SOX
- Collaborate with our IT department for tax settings of Finance IT Systems (e.g. ERP)
- Increase efficiency for existing processes and design new processes (e.g. use of AI)
- Contribute to GitLab’s international expansion activities (e.g. entity setup)
- Collaborate with GitLab Team Members on day-to-day activities, requests and projects
- Contribute to the overall finance & tax OKR’s
- Exposure to C-suite for larger global projects (e.g. Tax Strategy)
- This position reports to the Director of Tax

#### US Tax Manager (Intermediate) Requirements

* Degree in Business Taxation (Master's / JD / CPA)
* Proven track record of personal growth in your career path
* 5-8 years experience in a Big 4 environment and/or Business environment
* Affinity with Software and/or SaaS in a high growth environment
* Demonstrate a hands-on approach and success in working in a team environment
* Working knowledge of multiple International tax areas
* Ability to managing multiple projects at the same time
* Ability to use GitLab

#### Senior US Tax Manager

The Senior US Tax Manager reports to [Director of Tax](/job-families/finance/director-of-tax/).

#### Senior US Tax Manager Job Grade

The Senior US Tax Manager is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Senior US Tax Manager Responsibilities

* Extends that of the US Tax Manager (Intermediate) responsibilities

#### Senior US Tax Manager Requirements

* Exends that of the US Tax Manager (Intermediate) requirements
- Degree in Business Taxation (Master's / JD / CPA)
- Proven track record of personal growth in your career path
- 8-12 years experience in a Big 4 environment and Business environment
- Successfully leading large projects and/or change management projects
- Strong influencing skills, high adaptability and analytical thinking
- Expert in US Tax and International tax 
- Leading multiple projects at the same time

### Staff US Tax Manager

The Staff US Tax Manager reports to [Director of Tax](/job-families/finance/director-of-tax/).

#### Staff US Tax Manager Job Grade

The Staff US Tax Manager is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Staff US Tax Manager Responsibilities

* Extends that of the Senior US Tax Manager responsibilities

#### Staff US Tax Manager Requirements

* Extends that of the Senior US Tax Manager requirements
* Degree in Business Taxation (Master's / JD / CPA)
* 15+ years experience in a Big 4 environment and Business environment
* Strong business judgment applied to tax and finance operational activities
* Proven international experience working with overseas operations
* Proven success with managing tax on a global scale within a fast pace environment
* Proven ability to drive change in tax processes with a technology driven mindset
* Demonstrated experience in mentoring and leading a distributed team
* Leadership experience in a software or global technology company
* Proven strategic and tactical vision to lead a high performing team

## Performance Indicators

* [Effective Tax Rate](https://about.gitlab.com/handbook/tax/performance-indicators/#effective-tax-rate-etr)
* [Budget vs. Actual](https://about.gitlab.com/handbook/tax/performance-indicators/#budget-vs-actual)
* [Audit Adjustments](https://about.gitlab.com/handbook/tax/performance-indicators/#audit-adjustments)
* [International Expansion](https://about.gitlab.com/handbook/tax/performance-indicators/#international-expansion)

### Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process.
* Qualified candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.

- 45 minute interview with the Director of Tax
- 30 minute interview with our International Tax Manager
- 30 minute interview with our Tax Accountant
- 30 minute interview with our Sr. Director, Controller

Additional details about our process can be found on our [hiring page](/handbook/hiring).
