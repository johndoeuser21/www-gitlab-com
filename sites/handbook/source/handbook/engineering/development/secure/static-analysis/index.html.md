---
layout: handbook-page-toc
title: "Static Analysis Group"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Static Analysis

The Static Analysis group at GitLab is charged with developing solutions which perform [Static Analysis Software Testing (SAST)](/direction/secure/static-analysis/sast/),

[Secret Detection](/direction/secure/static-analysis/secret-detection/), and [Malware Detection](/direction/secure/#malware-scanning) for customer software repositories.

## Common Links

* Slack channel: #g_secure-static-analysis
* Slack alias: @secure_static_analysis_be
* Google groups: static-analysis-be@gitlab.com

## How We Work

The Static Analysis group is largely aligned with GitLab's [Product Development Flow](/handbook/product-development-flow/), however there are some notable differences in 
how we seek to deliver software. The backend engineering team predominantly concerns itself with the delivery of software, which is the portion of the workflow states where 
we deviate the most. What follows is how we manage the handoff from product management to engineering to deliver software.

Issues worked by this team are backend-centric and can span analyzers, vendored templates, and GitLab's Rails monolith. At times, issues can require support from Secure's 
frontend team if UI changes are required. Issues needing frontend support may [require more notice](/handbook/engineering/development/secure/fe-secure#How-to-work-with-us), and 
should be called out as early as possible.

### Issue Boards

* [Static Analysis Delivery Board](https://gitlab.com/groups/gitlab-org/-/boards/1590112?label_name[]=group%3A%3Astatic%20analysis&group_by=epic&label_name[]=backend)
  * Primary board for engineers from which engineers can work. It's stripped down to only include the workflow labels we use when delivering software and utilizes epic-level swimlanes.
* [Static Analysis Planning Board](https://gitlab.com/groups/gitlab-org/-/boards/1229162?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Astatic%20analysis)
  * Milestone-centric board primarily used by product management to gauge work in current and upcoming milestones.
* [Static Analysis EM Board](https://gitlab.com/groups/gitlab-org/-/boards/1655697)
  * Engineer-centric board used by engineering management to gauge how heavy a load engineer is carrying. Judged by the number of issues assigned to them.

### It all starts with planning

Like the rest of GitLab, we are product-driven and work in response to the priorities identified by Product Management. We use planning issues to articulate the epics which should be our top priorities in each release. This practice means we can interpret epics to be the features we're being asked to deliver and 
are given the freedom to break down those epics according to our best judgment. 

#### How we interact with planning issues

* Engineering Manager will mention engineers in planning issues to declare which epic they will work within.
* Engineering Manager will assign engineer(s) who will be working on issues in the prioritized epics.
* Engineering Manager will pull all issues on the epics prioritized into the `~workflow::planning breakdown` state.
  * This action should make the issues available on **Static Analysis Delivery Board** mentioned above.

### Software delivery in Static Analysis

While we follow GitLab's product development flow, our processes as a backend engineering team most closely resemble kanban. Engineers are empowered to choose issues from the Delivery 
Board in their assigned epic swimlane and pull them through the identified states. In addition to the workflow states identified by the company, we are experimenting with the 
`~workflow::refinement` state. Engineers are expected to use their best judgment as to how issues flow through the board, but the following outcomes are expected at each state.

| State | Expected Outcomes |
| ----- | ----------------- |
| `~workflow::planning breakdown` | - Issues deemed complete and understood.<br />- Issue split into smallest units of value.<br />- We try to split issues vertically rather than horizontally. Splitting vertically means the whole system will do something noticeably different; splitting horizontally results in trying to realize the fullest possible change in an individual component.<br />- If the issue can - and should - be split into separate issues, engineers are empowered to create the new issues, attach them to the epic they are working, and collaborate with product management on if they are included in current scope. |
| `~workflow::refinement` | - Implementation plan<br />- Relative size applied as weight. |
| `~workflow::ready for development` | Buffer queue - issue deemed to be `~Deliverable`, `~Stretch`, or possibly punted to a future iteration. |
| `~workflow::in dev` | Last MR is up and out of Draft or WIP status. |
| `~workflow::in review` | Last MR is merged and changes are available in a production environment. |
| `~workflow::verification` | Changes functionally tested in a production environment. |

#### Weights

Weights are used as a *rough* order of magnitude to help signal to the rest of the team how much work is involved.
Weights should be considered an output of the refinement process rather than its purpose.

The weighting system roughly aligns the scales used by other teams within GitLab. However, we use relative sizing rather than
assigning time estimates to possible values. A curated set of reference issues have been provided below, which will be updated periodically
to keep examples as current as possible.

##### Possible Values

It is perfectly acceptable if items take longer than the initial weight. We do not want to inflate weights,
as [velocity is more important than predictability](/handbook/engineering/#velocity-over-predictability) and weight inflation over-emphasizes predictability.

| Weight | Description | Reference issues |
| ------ | ----------- | ---------------- |
| 1 | Trivial task | [Update Bandit analyzer to v1.6.2](https://gitlab.com/gitlab-org/gitlab/-/issues/12926) |
| 2 | Small task | [Security Dashboard should show dismissal details on issues](https://gitlab.com/gitlab-org/gitlab/-/issues/9715) |
| 3 | Medium task | [Dependency Scanning Fails: "engine 'node' is incompatible with this module"](https://gitlab.com/gitlab-org/gitlab/-/issues/12471), [Dependency List contains duplicates (npm project)](https://gitlab.com/gitlab-org/gitlab/-/issues/12162), [Support setup.py in Dependency Scanning](https://gitlab.com/gitlab-org/gitlab/issues/11244), [Make vulnerability-details receive a vulnerability as a prop](https://gitlab.com/gitlab-org/gitlab/-/issues/14006) |
| 5 | Large task | [Engineering Discovery: reconsider Gemnasium client/server architecture](https://gitlab.com/gitlab-org/gitlab/issues/12930) |
| 8 | Extra-large task | [SAST for Apex](https://gitlab.com/gitlab-org/gitlab/-/issues/10680), [Add License information to the Dependency List - add license info backend](https://gitlab.com/gitlab-org/gitlab/issues/13084), [WAF statistics reporting](https://gitlab.com/gitlab-org/gitlab/-/issues/14707) |
| 13 | Extra-extra-large task | [Add support for REST API scans to DAST](https://gitlab.com/gitlab-org/gitlab/-/issues/10928) |
| Bigger | Epic in disguise |  |

### We Own What We Ship

We are responsible for delivering GitLab's SAST and Secret Detection features, and the analyzers we develop rely heavily upon open source software. 
This means we can be dramatically affected by changes in those software packages. We will check for updates to these packages once per [GitLab 
release](https://about.gitlab.com/releases/). New versions will be scrutinized for the following aspects:

* Breaking changes
* New, updated, or removed security rules
* Behavior changes
* Analyzer changes needed to use the new version
* Security vulnerabilities

An issue will be created and prioritized if a breaking change is discovered. Otherwise, dependency updates will be detailed in the relevant 
analyzer's changelog and a new version will be released utilizing the change. This is a lot of work, most likely requiring several hours of 
focused study to understand what is happening in the new version. As a result, dependency updates will be divided evenly and assigned to 
Senior and Intermediate Backend Engineers, with the remainder going to the group's Staff Backend Engineer. Assignments will be managed 
through our [Release project's issue template](https://gitlab.com/gitlab-org/security-products/release/-/blob/master/scripts/templates/release_issue.md.erb).

The assigned backend engineer is the group's primary liaison with the dependency's open source community. Engineers are expected to contribute 
back to those projects, especially if critical or high security findings are confirmed.

#### Testing for security vulnerabilities

We have a [dependencies group](https://gitlab.com/gitlab-org/security-products/dependencies) which contains mirrored copies of the OSS projects upon which we most rely. Prior to submitting an MR updating an analyzer to a new version of these projects, engineers are expected to do the following:

1. Find a release branch which matches the new version we wish to ship.
  * If one doesn't exist, create it from the corresponding tag.
1. Push the branch through a pipeline which executes all of our security products.
  * Please note, some of these projects have complicated builds. Auto-Devops works sometimes, but projects such as [spotbugs](https://gitlab.com/gitlab-org/security-products/dependencies/spotbugs) can require a custom CI configuration for our scans to be successful. Also, these projects include tests that can be noisy if not filtered out.
1. Evaluate any potential security vulnerabilities which are found. 
  * Work with the relevant Open Source community to resolve any Critical or High severity findings.
  * GitLab has published [Secure Coding Guidelines](https://docs.gitlab.com/ee/development/secure_coding_guidelines.html), which may be a useful resource to use when trying to solve identified risks.

We do not want to ship updated dependencies which have Critical and High severity vulnerabilities in them. If we find ourselves in this situation, we will 
withhold updates to the dependency until the problems have been patched.

#### Go security fixes

At times we will need to update our analyzers because of security updates to golang itself. In this situation, we follow the [established release process](https://about.gitlab.com/handbook/engineering/development/secure/release_process.html#security-fixes-of-go).

### Community Contributions

The Static Analysis group is actively reserving capacity each iteration to be responsive to MRs from the community. Each backend engineer 
in the group will serve as the [Community Merge Request Coach](https://about.gitlab.com/job-families/expert/merge-request-coach/) on a rotating 
basis. The Community Merge Request Coach has the following responsibilities, in priority order:

1. Triage and work with community contributors to help drive their MRs to completion.
1. Release feature(s) to core.
1. Triage and resolve ~priority::1 bugs.
1. Work an issue in the backlog that's of great interest to you.

### Product Prioritization Labels

We also use additional labels to categorize different types of requests. These labels represent the top areas of product impact we are currently focused on within the Static Analysis team.

[Issue board](https://gitlab.com/gitlab-org/gitlab/-/boards/1578273?label_name[]=group%3A%3Astatic%20analysis).

#### `~SAST: Common Need`

Features we expect everyone to need and use

*Goal:* How do we protect from the most common security issues

*Measure:* Opportunity for impact

##### Types of issues

* Scanner updates
* Language coverage
* OWASP Top 10
* Better Vunl Metadata
* Documentation

#### `~SAST: Advanced Config`

Features we don’t expect everyone to use

*Goal:* Enable customization in configuration and enable advanced capabilities advanced users

*Measure:* Power and flexibility

##### Types of issues

* Customize rulesets
* Monorepo support
* Security scan customization

#### `~SAST: Enforce & Control`

Use least disruptive settings by default and allow customizations

*Goal:* Provide robust policies and controls to enforce security compliance

*Measure:* Policy & Compliance

##### Types of issues

* New scanners
* Policy ideas
* Compliance features

#### `~SAST: Workflow`

*Goal:* Enable workflows to ensure the appropriate attention on issues and allowing them to be tracked overtime.

*Measure:* Trust Scanner Issues & Track over time

##### Types of issues

* Speedy Scanners
* Usage ping data

#### `~SAST: Integrate`

Strongly defined integration harness to make internal/external integrations easier and more conformant

*Goal:* Provide defined integration point, enabling easier integrations

*Measure:* Be an ecosystem player

##### Types of issues

* Integration related ideas
